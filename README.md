## Basic Idea

Backend API for Unqork Design Tool that has two main functions.

1. Accept JSON object containing css custom properties and some metadata about the environment and theme, create a css file and store it on the server in folder `css/`.
2. Serve customer css files from static `css/` folder

Additionally, the customer.css file in the below example can be fetched with a standard HTML stylesheet link.

```html
<link
  rel="stylesheet"
  href="https://example-cdn.com/uqss/$env/$theme/customer.css"
/>
```

---

## Usage:

```bash
node index.js
# Unqork Design Tool - Backend API
# Listening on port 8080
```

Assuming this would run on 80 in production.

---

## POST Endpoint

Request Body JSON:

```json
{
  "stylesObject": {
    "--unq-background-color": "#cecece",
    "--unq-text-color": "#222222"
    ...
  },
  "env": "styles-staging",
  "theme": "uqss-base"

```

Respose: "Success"

---

## GET Endpoint

Request URL: `https://example-cdn.com/uqss/$env/$theme/customer.css`

Response: customer.css file

---

## Thoughts on Integration

While this could eventually be the actual implementation for the backend of this WYSIWYG design tool, the original intention of this API is to service the POC created by Alex & Dylan in the context of a single CoE client. Our short-term goal is to host this application on an Unqork-owned aws instance so we can begin to test this functionality within designer. Once we are confident in the functionality of the tool we can then look into what the GA release plan will look like.

@Marc, is there any way we can deploy this in the next week or so? (Knowing this iteration is not permanent).

I assume we can come up with a more elegant solution at scale for all of our clients, and maybe we can even simplify the core functionality. I'm definitely interested in what you think and setting up a brainstorm session to think through long-term plans.
