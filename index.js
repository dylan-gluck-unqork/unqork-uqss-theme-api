// Imports
const fs = require("fs");
const path = require("path");
const express = require("express");
const styleToCss = require("style-object-to-css-string");

// Setup Express
const app = express();
const port = 8080;

// Create CSS Directory if does not exist
const baseDir = path.join(__dirname, "/css");
if (!fs.existsSync(baseDir)) fs.mkdirSync(baseDir);

// Accept JSON
app.use(express.json());

// GET Endoint
// Serve static CSS files
// Eg: https://cdn-host.com/uqss/$environment/$theme/customer.css
app.use("/uqss", express.static("css"));

// POST Enpoint
// Accepts JSON object with css custom properties
// Creates customer.css file and saves to disk
app.post("/", function (req, res) {
  const stylesObject = req.body.stylesObject;
  const env = req.body.env;
  const theme = req.body.theme;

  if (!stylesObject || !env || !theme) {
    throw new Error(
      "Data object formatted incorrectly. Please include stylesObject, env and theme"
    );
  }

  const envPath = path.join(baseDir, env);
  const themePath = path.join(envPath, theme);
  const cssPath = path.join(themePath, "customer.css");

  if (!fs.existsSync(envPath)) fs.mkdirSync(envPath);
  if (!fs.existsSync(themePath)) fs.mkdirSync(themePath);

  let styleString = `:root{ ${styleToCss(stylesObject)} }`;

  fs.writeFile(cssPath, styleString, (err) => {
    if (err) {
      console.log(err);
      return;
    }
  });

  res.send("Success");
});

// Start API
app.listen(port, () => {
  console.log(`Unqork Design Tool - Backend API`);
  console.log(`Listening on port ${port}`);
});
